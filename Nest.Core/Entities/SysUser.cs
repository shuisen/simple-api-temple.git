﻿using System;

namespace Nest
{
    /// <summary>
    /// 用户
    /// </summary>
    public class SysUser
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }
    }
}
