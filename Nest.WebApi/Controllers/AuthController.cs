﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nest.Controllers
{
    /// <summary>
    /// 认证
    /// </summary>
    [ApiController]
    [Route("api/Auth")]
    public class AuthController : ControllerBase
    {
        /// <summary>
        /// 测试注册
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public SysUser Get()
        {
            return new SysUser { UserName = "刘备" };
        }
    }
}
